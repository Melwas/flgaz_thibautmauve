# Project FlGaz	
	_Designed by Thibaut Mauve and Florian Wagnier._

This app is designed to provide you an instant and functional chatting web-site.
The functions integrated in FlGaz will grant you a free space for chatting anonymously and constraints free.
We are committed to guarantee total freedom to our users without any subscription or registration. If you want to post your own philosophy of life, you can doing it here, and everyone could judge you by your saying and not by the number of your “followers”.

## Getting Started:
### Prerequisites:

This application is running on Python 3, so 3.7 is required to run it.
You also need to install Flask version 1.8

### Installing:

- Install Flask ;
- Download repositories from our GitLab.

### Test:
You'll be able to ping your website and you can also post your first comment on the main page!

### Authors:
- Mr Mauve Thibaut
- Mr Wagnier Florian
- Mr Anthony & Mr Jonathan