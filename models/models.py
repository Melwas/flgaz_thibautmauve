class User(db.Model):
    __tablename__ = 'utilisateur'

    user_id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.String, unique=True, nullable=False)
    user_password = db.Column(db.String, nullable=False)

    def __init__(self, user_name=None, user_password=None):
        self.user_name = user_name
        self.user_password = user_password

    def __repr__(self):
        return '<User {0}>'.format(self.user_name)



class Commentaire(db.Model):
    __tablename__ = 'commentaire'

    commentaire_id = db.Column(db.Integer, primary_key=True)
    commentaire_utilisateur_id = db.Column(db.Integer, db.ForeignKey('utilisateur.user_id'))
    commentaire_content = db.Column(db.String, nullable=False)

    def __init__(self, commentaire_id, commentaire_utilisateur_id):
        self.commentaire_id = commentaire_id
        self.commentaire_utilisateur_id = commentaire_utilisateur_id

    def __repr__(self):
        return '<Id {0} - {1}>'.format(self.commentaire_id, self.commentaire_content)