from wtforms import StringField
from wtforms.validators import DataRequired, Length, AlphaNumeric
from flask_wtf import FlaskForm

class CommentaireForm(FlaskForm):
    name = StringField(
        'username',
        validators=[DataRequired(), Length(max=20)]
    )
    commentaire = StringField(
        'commentaire',
        validators=[DataRequired(), Length(max=200)]
    )