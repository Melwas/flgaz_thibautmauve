from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Length
from flask_wtf import FlaskForm

class LoginForm(FlaskForm):
    username = StringField(
        'username',
        validators=[DataRequired(), Length(min=6, max=25)]
    )
    password = PasswordField(
        'password',
        validators=[DataRequired()]
    )