from flask import Flask, request, render_template, redirect, url_for, render_template, request, session
import csv
from forms.login_form import LoginForm

app = Flask(__name__)
app.config.from_pyfile('_config.py')

"""
    Function add_headers() : Adds headers to the HTTP response.
"""
@app.after_request
def add_headers(response):
  response.headers['Cache-Control'] = 'max.age=300'
  response.headers['Access-Control-Allow-Origin'] = 'http://thibaut.eu.pythonanywhere.com'

  return response


"""
    Function home() : Checks if the person trying to connect is registered
    in session. If so, it redirects to the message form.
"""
@app.route('/')
def home():
  form = LoginForm(request.form)
  if not session.get('logged_in'):
      return render_template('login.html', form=form)
  else:
	  return render_template('formulaire.html')


"""
    Function save_gazouille() : Allows to send messages and register them in a
    CSV file : gazouilles.csv
"""
@app.route('/gaz', methods=['GET','POST'])
def save_gazouille():
	if request.method == 'POST':
		print(request.form)
		if len(request.form['user-text']) <= 200 :
		  dump_to_csv(request.form)
		return redirect(url_for('timeline'))
		#return "OK"
	if request.method == 'GET':
		return render_template('formulaire.html')

"""
    Function timeline() : Displays all the posted messages.
"""
@app.route('/timeline', methods=['GET'])
def timeline():
  gaz = parse_from_csv()
  return render_template("timeline.html", gaz = gaz)


"""
    Function timelineForUser() : Displays all the posted messages written by the
    specified user in the URL.
"""
@app.route('/timeline/<username>/', methods=['GET'])
def timelineForUser(username):
  gaz = parse_from_csv_for_user(username)
  return render_template("timeline.html", gaz = gaz)

def parse_from_csv_for_user(username):
  gaz = []
  with open('/home/thibautmauve/mysite/gazouilles.csv', 'r') as f:
	  reader = csv.reader(f)
	  for row in reader:
		  if row[0] == username:
			  gaz.append({"user":row[0], "text":row[1]})
  return gaz

@app.route('/login', methods=['GET', 'POST'])
def do_admin_login():
  if request.method == 'POST':
	  if request.form['password'] == 'password' and request.form['username'] == 'admin':
		  session['logged_in'] = True
	  else:
		  return render_template('404.html')

  return home()

def parse_from_csv():
  gaz = []
  with open('/home/thibautmauve/mysite/gazouilles.csv', 'r') as f:
	  reader = csv.reader(f)
	  for row in reader:
		  gaz.append({"user":row[0], "text":row[1]})
  return gaz

def dump_to_csv(d):
  donnees = [d["user-name"],d["user-text"] ]
  with open('/home/thibautmauve/mysite/gazouilles.csv', 'a', newline='', encoding='utf-8') as f:
	  writer = csv.writer(f)
	  writer.writerow(donnees)