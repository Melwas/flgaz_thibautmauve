# Rapport : prise de connaissances sur l'environnement et le projet FLGAZ

## 1) Découverte des outils
### Que propose le site PythonAnywhere.com ?

_pythonanywhere.com_ est un site web proposant des machines hébergeant un environnement de développement complet en langage Python pour la programmation, le déploiement et l'hébergement à terme de sites web. Il est également proposé des cours en ligne pour permettre aux moins aguerris en Python de développer leur compétence dans ledit langage.

### Qu'est ce que FLASK ? Quels sites connus utilisent Flask ?

_Flask_ est un microframework ("micro" car il ne fait pas l'intégralité de ce que ferait un framework web comme Django sans extensions) pour le développement d'applications web en langage Python. Il permet d'implémenter des comportements employant le protocole web HTTP.

_Qui utilise Flask ?_

Aujourd'hui plusieurs applications et sites connus emploient Flask pour répondrent à certains besoins :
- Netflix emploie Flask pour leur framework Lemur ;
- Reddit s'en est servi pour certains scripts encore visibles en read-only sur GitHub ;
- Uber se sert de Flask pour la gestion de ses partenaires (= chauffeurs).

## 2) Description des actions réalisées

### Quelles étapes avez-vous suivi ?

1ère étape : Création d'un compte sur pythonanywhere.com
2e étape : Téléchargement de FLGAZ via la plateforme outofpluto.com
3e étape : Initialisation d'un repository Git en local pour le projet extrait de l'archive
4e étape : Création d'un repository sur GitLab

### Quelles difficultés avez-vous rencontrées ?

Suite à une mauvaise initialisation du repository public sur GitLab, j'ai éprouvé des difficultés à déposer le projet dessus. Le problème est résolu (merci beaucoup Anthony).

## Réflexions sur le projet

### Quels sont, selon vous, les aspects techniques limitants du projet FLGAZ dans l'état initial ?

Les jeux de données de test sont en CSV. Des données au format JSON représenterait, à mon humble avis, une opportunité d'approfondir davantage la connaissance de Python. De plus le format JSON apparaît être fonctionnellement plus ordonné car plus explicite que le format plat où l'on devine les métadonnées de ce qui nous est présenté.

### Quelles sont, selon vous, les menaces auxquelles un tel projet peut être soumis ?

En l'état, FLGAZ n'est en rien protégé face aux commentaires au contenu hautement discutable ainsi qu'à l'usurpation d'identité des utilisateurs.